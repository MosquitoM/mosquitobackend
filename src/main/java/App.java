import api.ContainerController;
import api.ContainerService;
import controller.*;

import java.sql.SQLException;
import java.util.*;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.shekhargulati.reactivex.docker.client.representations.ContainerInspectResponse;
import com.shekhargulati.reactivex.docker.client.representations.DockerContainerRequest;
import com.shekhargulati.reactivex.rxokhttp.ServiceException;
import com.spotify.docker.client.DockerCertificateException;
import com.spotify.docker.client.DockerException;
import database.DatabaseSingleton;
import db_services.NodeService;
import model.Container;
import model.Node;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static spark.Spark.get;
import static spark.Spark.staticFileLocation;
/**
 * Created by Yusuf on 23-4-2016.
 * All the endpoint and their serivces
 */
public class App {

    public static void main(String[] args) throws SQLException {
        staticFileLocation("/public");
        String layout = "templates/layout.vtl";
        JdbcConnectionSource connectionSource = DatabaseSingleton.getConnection();
        // Homepage
        get("/", (request, response) -> {
            Map model = new HashMap();
            model.put("template", "templates/index.vtl");
            return new ModelAndView(model, layout);
        }, new VelocityTemplateEngine());

        get("/crud", (request, response) -> {
            Map model = new HashMap();
            model.put("template", "templates/crud.vtl");
            try {
                String action = request.queryParams("action");
                String nodeId = request.queryParams("nodeId");
                String containerId = request.queryParams("containerId");


                OverviewContainers overviewContainers = new OverviewContainers();
                CrudContainer crudContainer = new CrudContainer();
                if ((action != null)) {
                    if (action.equals("deleteContainer")) {
                        crudContainer.deleteContainer(containerId, nodeId);
                    }
                }
                model.put("template", "templates/crud.vtl");
                model.put("containersActive", overviewContainers.getActiveContainers());
                model.put("containersPassive", overviewContainers.getPassiveContainers());
            } catch (Exception e) {
                e.printStackTrace();
            }

            return new ModelAndView(model, layout);
        }, new VelocityTemplateEngine());

        new ContainerController(new ContainerService());
        new MigrateController(new MigrateService());
        new NodeController();

        new CloudController();


        //Make the page where the actions of :
        // Scaling
        // Start
        // stop
        // Info
        // Create
        // Are shown and handled
        get("/containerInfo", (request, response) -> {

            Map model = new HashMap();

            try {

                String containerId = request.queryParams("containerId");
                String action = request.queryParams("action");
                String imageId = request.queryParams("imageId");
                String containerName = request.queryParams("containerName");
                String exposedPorts = request.queryParams("exposedPorts");
                String hostPorts = request.queryParams("hostPorts");
                String nodeId = request.queryParams("nodeId");

                String actionName = "";
                ContainerInspectResponse containerInfo = null;
                DockerContainerRequest containerInfoConfig = null;
                List<String> containerInfoConfigEnv = null;

                CrudContainer crudContainer = new CrudContainer();

                if (action != null) {
                    if (action.equals("createContainer")) {

                        containerId = crudContainer.createContainer(imageId, nodeId, containerName, exposedPorts, hostPorts);
                        actionName = "Container Created";
                    }
                    if (action.equals("startContainer")) {

                        crudContainer.startContainer(containerId, nodeId);
                        actionName = " Container Started";
                    }
                    if (action.equals("stopContainer")) {

                        crudContainer.stopContainer(containerId, nodeId);
                        actionName = "Container Stop";
                    }
                    if (action.equals("scaleContainer")) {

                        Scale scale = new Scale();

                        imageId = scale.commitImage(containerId, nodeId);
                        scale.saveImage(imageId, nodeId);
                        nodeId = scale.getConnection();
                        containerId = scale.loadImage(imageId);
                        System.out.println(containerId);
                        actionName = "Container scaled";
                    }
                }


                if (containerId.equals("Service returned 404 with message Not Found")) {
                    actionName = "The container couldn't be created because the image doesn't exist";

                } else if (containerId.equals("Service returned 409 with message Conflict")) {
                    actionName = "The container couldnt be created because a container already exist with that name on this node";

                } else {
                    containerInfo = crudContainer.readContainer(containerId, nodeId);

                    if (containerInfo == null) {
                        actionName = "The container couldn't be found. Please check if this container exist";
                    } else {
                        containerInfoConfig = containerInfo.config();
                        containerInfoConfigEnv = containerInfoConfig.getEnv();
                        crudContainer.getLogs(containerId, nodeId);
                    }
                }

                model.put("template", "templates/containerInfo.vtl");
                model.put("containerInfo", containerInfo);
                model.put("action", actionName);
                model.put("containerInfoConfig", containerInfoConfig);
                model.put("containerInfoConfigEnv", containerInfoConfigEnv);

            } catch (SQLException | DockerException | InterruptedException | DockerCertificateException e) {
                e.printStackTrace();
            }
            return new ModelAndView(model, layout);
        }, new VelocityTemplateEngine());


        //page where you can create a container
        get("/createContainer", (request, response) -> {
            Map model = new HashMap();

            try {

                Dao<Node, String> nodeDAO = DaoManager.createDao(connectionSource, Node.class);
                List<Node> nodeList = nodeDAO.queryForAll();

                model.put("nodeList", nodeList);
                model.put("template", "templates/createContainer.vtl");


            } catch (SQLException e) {
                e.printStackTrace();
            }
            return new ModelAndView(model, layout);
        }, new VelocityTemplateEngine());


        // Scale containers among the nodes
        get("/scale", (request, response) -> {
            Map model = new HashMap();

            try {

                OverviewContainers overviewContainers = new OverviewContainers();
                model.put("template", "templates/crud.vtl");
                model.put("containersActive", overviewContainers.getActiveContainers());

            } catch (SQLException e) {
                e.printStackTrace();
            }
            model.put("template", "templates/form/scale_form.vtl");
            return new ModelAndView(model, layout);
        }, new VelocityTemplateEngine());
    }
}