package config;

import org.junit.Test;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import static org.junit.Assert.*;

/**
 * Created by jahmale on 8-6-2016.
 */
public class ConfigTestClass {

    private String testPasswordValue = "testPasswordVal";
    private String testUsernameValue = "testUsernameVal";

    private Properties properties = new Properties();
    private String fileName = "debug_config.properties";


    @Test
    public void trySaveCredentials(){
        try{
            properties.setProperty("debug_user", testUsernameValue);
            properties.setProperty("debug_key", testPasswordValue);

            OutputStream out = new FileOutputStream(fileName);
            properties.store(out,"saving new property");
        }catch(Exception e){
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    @Test
    public void tryGetCredentials(){
        try{
            trySaveCredentials();
            assertEquals(testUsernameValue, properties.getProperty("debug_user"));
            assertEquals(testPasswordValue, properties.getProperty("debug_key"));
        }catch(Exception e){
            fail(e.getMessage());
        }
    }

    @Test
    public void tryEncryption(){
        try{
            String preAdjusted = testPasswordValue;
            String postAdjusted = Config.getInstance().encryptPassword(testPasswordValue.toCharArray());
            System.out.println(postAdjusted);
            assertTrue(preAdjusted != postAdjusted);

            String decrypted = Config.getInstance().decryptPassword(postAdjusted.toCharArray());
            System.out.println(decrypted);
            assertTrue(decrypted.equals(preAdjusted));
        }catch(Exception e){
            e.printStackTrace();
            fail(e.getMessage());
        }
    }
}
