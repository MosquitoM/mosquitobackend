package config;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import com.sun.org.apache.xml.internal.security.utils.JavaUtils;
import javafx.util.Pair;
import org.apache.commons.codec.DecoderException;
import org.bouncycastle.util.encoders.UrlBase64Encoder;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.security.AlgorithmParameters;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import static org.apache.commons.codec.binary.Hex.decodeHex;
import static org.apache.commons.codec.binary.Hex.encodeHex;

/**
 * Created by jahmale on 7-6-2016.
 */
public class Config {

    private final static String fileName= "config.properties";
    private final static String cloud_username = "cloud_username";
    private final static String cloud_password = "cloud_password";
    private final static String cloud_crypto_key = "hexKey";
    private final static String iv_bytes_key = "IVbyte";

    private static Config config;

    private static int iterations = 65536  ;
    private static int keySize = 256;
    private static byte[] ivBytes;

    private InputStream ip;
    private FileWriter writer;
    private File configFile;
    private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    protected static Properties properties;

    private Config(){
        setParams();
    }

    /**
     * Will return an instance of Config
     * @return if there was not one available, a new instance will be returned otherwise the pre-existing instance will be returned
     */
    public static Config getInstance(){
        if(config == null){
            config = new Config();
        }
        return config;
    }

    /**
     * get the current credentials that have been placed inside the config
     * @return a java Pair object containing the credentials, will return null if there are none
     */
    public Pair<String, String> getCredentials(){
        try{
            String usernameValue = properties.getProperty(cloud_username);
            String passwordValue = decryptPassword(properties.getProperty(cloud_password).toCharArray());
            return new Pair<>(usernameValue, passwordValue);
        }catch(Exception e){
            e.printStackTrace();
            System.err.println("No credentials available");
            return null;
        }
    }

    /**
     * save a set of credentials, if there already is one in the config File, that set will be replaced by the current one
     * @param credentials a java pair object with as key: the username and as value the password e.g: new Pair<>("username", "secretPassword1")
     * @throws IOException
     * @throws Exception
     */
    public void saveCredentials(Pair<String, String> credentials)throws Exception{
        String usernameValue = credentials.getKey();
        String passwordValue = encryptPassword(credentials.getValue().toCharArray());
        properties.setProperty(cloud_username, usernameValue);
        properties.setProperty(cloud_password, passwordValue);
        writer = new FileWriter(configFile);
        properties.store(writer,"saving auth "+dateFormat.format(new Date()));
    }

    /**
     * removes the credentials from the config file
     * @throws Exception
     */
    public void removeCredentials() throws Exception{
        properties.remove(cloud_username);
        properties.remove(cloud_password);
        properties.remove(cloud_crypto_key);
        properties.remove(iv_bytes_key);
        writer = new FileWriter(configFile);
        properties.store(writer,"removing previous auth");
    }

    private void setParams(){
        configFile = new File(fileName);
        try{
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            ip = new FileInputStream(fileName);
            properties = new Properties();
            if(ip != null){
                properties.load(ip);
            }
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    protected String encryptPassword(char [] userPW) throws Exception{
        byte[] saltBytes = createSalt().getBytes();

        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");

        PBEKeySpec keySpecifications = new PBEKeySpec(userPW, saltBytes, iterations, keySize);
        SecretKey secretKey = keyFactory.generateSecret(keySpecifications);

        SecretKeySpec secretKeySpecification = new SecretKeySpec(secretKey.getEncoded(), "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpecification);

        AlgorithmParameters parameters = cipher.getParameters();
        ivBytes = parameters.getParameterSpec(IvParameterSpec.class).getIV();
        System.out.println(ivBytes);

        String decodedIV = Base64.encode(ivBytes);

        saveSecretKey(secretKey, decodedIV);
        byte[] encryptedTextBytes = cipher.doFinal(String.valueOf(userPW).getBytes("UTF-8"));

        return DatatypeConverter.printBase64Binary(encryptedTextBytes);
    }

    protected String decryptPassword(char[] encryptedText) throws Exception {
        byte[] encryptedTextBytes = DatatypeConverter.parseBase64Binary(new String(encryptedText));
        SecretKey secretKey = getSecretKey();
        System.out.println(ivBytes);

        SecretKeySpec secretSpecification = new SecretKeySpec(secretKey.getEncoded(), "AES");

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, secretSpecification, new IvParameterSpec(ivBytes));

        byte[] decryptedTextBytes = cipher.doFinal(encryptedTextBytes);
        return new String(decryptedTextBytes);
    }

    private SecretKey getSecretKey()throws Exception{
        String keyData = properties.getProperty(cloud_crypto_key);
        String decoded = properties.getProperty(iv_bytes_key);
        ivBytes = decoded.getBytes("ISO-8859-1");
        ivBytes = Base64.decode(decoded);
        char[] hex = keyData.toCharArray();
        byte[] encodedSecretKey = decodeHex(hex);
        return new SecretKeySpec(encodedSecretKey, "AES");
    }

    private void saveSecretKey(SecretKey secretKey, String decodedIV){
        String keyData = encryptSecretKey(secretKey.getEncoded());
        properties.setProperty(cloud_crypto_key, keyData);
        properties.setProperty(iv_bytes_key, decodedIV);
    }

    protected String encryptSecretKey(byte[] encodedSecretKey){
        char[] hex = encodeHex(encodedSecretKey);
        String keyData = String.valueOf(hex);
        return keyData;
    }

    private String createSalt() throws Exception{
        SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[20];
        secureRandom.nextBytes(salt);
        return salt.toString();
    }
}
