package exceptions;

/**
 * Created by jahmale on 12-6-2016.
 */
public class MosquitoFailedRequestException extends Exception {

    public MosquitoFailedRequestException(){}

    public MosquitoFailedRequestException(String message){
        super(message);
    }

}
