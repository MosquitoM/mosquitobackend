package database;

import com.j256.ormlite.jdbc.JdbcConnectionSource;

import java.sql.SQLException;

/**
 * Created by Yusuf on 19-5-2016.
 */
public class DatabaseSingleton {
    private static DatabaseSingleton JDBC;
    private static String DATABASE_URL;
    private static String USERNAME;
    private static String PASSWORD;
    private static JdbcConnectionSource CONNECTION;

    private DatabaseSingleton(){
        DATABASE_URL = "jdbc:postgresql://localhost:5432/ictlab";
        USERNAME = "postgres";
        PASSWORD = "0000";
        CONNECTION = setConnection();
    }

    /**
     * Checks if the Java database connection is initialized and sets the connection.
     * @return JDBC using ORMLite library
     */
    private static JdbcConnectionSource setConnection(){
        JdbcConnectionSource connectionSource = null;
        try {
            connectionSource = new JdbcConnectionSource(DATABASE_URL, USERNAME, PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connectionSource;
    }

    /**
     * Returns Java database connection
     * @return Java database connection using ORMLITE library
     */
    public static JdbcConnectionSource getConnection(){
        if(JDBC == null){
            JDBC = new DatabaseSingleton();
        }
        return CONNECTION;
    }

}
