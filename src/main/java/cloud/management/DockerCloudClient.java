package cloud.management;

import api.ClientCaller;
import config.Config;
import javafx.util.Pair;
import model.DockerCloudContainer;
import model.DockerCloudNode;
import model.DockerCloudService;
import org.json.JSONObject;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Created by jahmale on 20-5-2016.
 */
public class DockerCloudClient implements DockerCloudBase, DockerCloudContainerActions,
            DockerCloudServiceActions, DockerCloudNodeActions{

    //todo: apply this static boolean to the webpage, if it is false, then the user will not be able to access dockercloud pages
    public static boolean hasDockerCloudCredentials;

    private static DockerUser currentUser;
    private static ClientCaller caller;
    private static DockerCloudContainer containerBluePrint;
    private static DockerCloudService serviceBluePrint;
    private static DockerCloudNode nodeBluePrint;
    private static DockerCloudClient instance;

    /**
     * creates a new DockerCloudClient, for use of the API calls, credentials need to be supplied with the addCredentials method
     * @return if there was no instance yet it will return a new one, otherwise the current one will be returned
     */
    public static DockerCloudClient getInstance() {
        //at this point the user should be made and as thus, this class should be fully functional
        if(instance == null){
            instance = new DockerCloudClient();
            caller = new ClientCaller(DOCKER_BASE_URL, "application/json");
            containerBluePrint = new DockerCloudContainer();
            serviceBluePrint = new DockerCloudService();
            nodeBluePrint = new DockerCloudNode();
        }
        return instance;
    }

    private DockerCloudClient(){}

    /**
     * adds the needed credentials, to make use of the DockerCloud API calls
     * @param username the dockerhub username connected to the dockercloud account of the current user
     * @param authCode the API_key of the dockercloud account that will be used, which can be found on the dockercloud account
     * @return if there was no DockerCloudClient created yet, it will create a new one with the current credentials, otherwise
     * it will update the currently existing DockerCloudClient with the current credentials
     */
    public DockerCloudClient addCredentials(String username, String authCode){
        currentUser = new DockerUser(username, authCode);
        hasDockerCloudCredentials = true;
        try{
            Config.getInstance().saveCredentials(new Pair<>(username, authCode));
        }catch(Exception e){
            System.out.println("could not save credentials");
        }
        return getInstance();
    }

    @Override
    public Boolean authenticate() {
        if(!hasDockerCloudCredentials) {
            return false;
        }

        String authenticate = String.valueOf(caller.getRequest(
                    DOCKER_SERVICE_PATH,
                    new Pair<>(DOCKER_AUTH_HEADER, currentUser.getAuthToken()),
                    null));
        if(authenticate.equalsIgnoreCase(INCORRECT_AUTH)){
            hasDockerCloudCredentials = false;
            return false;
        }else{
            return true;
        }
    }

    @Override
    public List<DockerCloudNode> listAllNodes() {
        JSONObject jsonService = caller.getRequest(
                DOCKER_NODE_PATH,
                new Pair<>(DOCKER_AUTH_HEADER, currentUser.getAuthToken()),
                null);
        List<DockerCloudNode> list = nodeBluePrint.createObjectList(jsonService.getJSONArray("objects"));
        return list;
    }

    //CONTAINER CALLS
    @Override
    public List<DockerCloudContainer> listAllContainers(){
        JSONObject jsonContainer = caller.getRequest(
                DOCKER_CONTAINER_PATH,
                new Pair<>(DOCKER_AUTH_HEADER, currentUser.getAuthToken()),
                null);
        return containerBluePrint.createObjectList(jsonContainer.getJSONArray("objects"));

    }

    @Override
    public List<DockerCloudContainer> listAllContainers(String filter) {
        JSONObject jsonContainers = caller.getRequest(
                DOCKER_CONTAINER_PATH,
                new Pair<>(DOCKER_AUTH_HEADER, currentUser.getAuthToken()),
                new Pair<>("service",DOCKER_SERVICE_PATH+"/"+filter+"/"));
        return containerBluePrint.createObjectList(jsonContainers.getJSONArray("objects"));
    }

    @Override
    public DockerCloudContainer getExistingContainer(String uuid) {
        JSONObject jsonContainer = caller.getRequest(
                DOCKER_CONTAINER_PATH+"/"+uuid,
                new Pair<>(DOCKER_AUTH_HEADER, currentUser.getAuthToken()),
                null);
        return containerBluePrint.createObject(jsonContainer);
    }

    @Override
    public DockerCloudContainer startContainer(String uuid) {
        JSONObject jsonContainer = caller.postRequest(
                DOCKER_CONTAINER_PATH+"/"+uuid+"/"+START,
                new Pair<>(DOCKER_AUTH_HEADER, currentUser.getAuthToken()),
                null);
        return containerBluePrint.createObject(jsonContainer);
    }

    @Override
    public DockerCloudContainer stopContainer(String uuid) {
        JSONObject jsonContainer = caller.postRequest(
                DOCKER_CONTAINER_PATH+"/"+uuid+"/"+STOP,
                new Pair<>(DOCKER_AUTH_HEADER, currentUser.getAuthToken()),
                null);
        //return DockerCloudContainer.createDCContainer(jsonContainer);
        return containerBluePrint.createObject(jsonContainer);
    }

    @Override
    public DockerCloudContainer terminateContainer(String uuid) {
        JSONObject jsonContainer = caller.deleteRequest(
                DOCKER_CONTAINER_PATH+"/"+uuid,
                new Pair<>(DOCKER_AUTH_HEADER, currentUser.getAuthToken()));
        //return DockerCloudContainer.createDCContainer(jsonContainer);
        return containerBluePrint.createObject(jsonContainer);
    }

    //SERVICE CALLS
    @Override
    public DockerCloudService createService(String JSONRequest){
        JSONObject jsonService = caller.postRequest(
                DOCKER_SERVICE_PATH,
                new Pair<>(DOCKER_AUTH_HEADER, currentUser.getAuthToken()),
                Entity.entity(JSONRequest, MediaType.APPLICATION_JSON_TYPE));
        return serviceBluePrint.createObject(jsonService);
    }

    @Override
    public List<DockerCloudService> listServices(){
        JSONObject jsonService = caller.getRequest(
                DOCKER_SERVICE_PATH,
                new Pair<>(DOCKER_AUTH_HEADER, currentUser.getAuthToken()),
                null);
        return serviceBluePrint.createObjectList(jsonService.getJSONArray("objects"));
    }

    @Override
    public DockerCloudService startService(String uuid) {
        System.out.println("executing: "+DOCKER_SERVICE_PATH+"/"+uuid+"/"+START);
        JSONObject jsonService = caller.postRequest(
                DOCKER_SERVICE_PATH+"/"+uuid+"/"+START,
                new Pair<>(DOCKER_AUTH_HEADER, currentUser.getAuthToken()),
                null);
        return serviceBluePrint.createObject(jsonService);
    }

    @Override
    public DockerCloudService stopService(String uuid) {
        JSONObject jsonService = caller.postRequest(
                DOCKER_SERVICE_PATH+"/"+uuid+"/"+STOP,
                new Pair<>(DOCKER_AUTH_HEADER, currentUser.getAuthToken()),
                null);
        return serviceBluePrint.createObject(jsonService);
    }

    @Override
    public DockerCloudService getExistingService(String uuid) {
        JSONObject jsonService = caller.getRequest(
                DOCKER_SERVICE_PATH+"/"+uuid,
                new Pair<>(DOCKER_AUTH_HEADER, currentUser.getAuthToken()),
                null);
        return serviceBluePrint.createObject(jsonService);
    }

    @Override
    public DockerCloudService terminateService(String uuid) {
        JSONObject jsonService = caller.deleteRequest(
                DOCKER_SERVICE_PATH+"/"+uuid,
                new Pair<>(DOCKER_AUTH_HEADER, currentUser.getAuthToken()));
        return serviceBluePrint.createObject(jsonService);
    }

    private static class DockerUser{
        public String username;
        public Client client;
        private String authToken;

        public DockerUser(String username, String keyCode){
            this.username = username;
            client = ClientBuilder.newClient();
            setAuthToken(username, keyCode);
        }

        public String getAuthToken(){
            if(authToken != null){
                System.out.println("auth is: "+authToken);
                return authToken;
            }else{
                System.err.println("could not UTF encode credentials");
                return null;
            }
        }

        private void setAuthToken(String username, String keyCode){
            String token = username+":"+keyCode;
            try{
                authToken =  "BASIC " + DatatypeConverter.printBase64Binary(token.getBytes("UTF-8"));
            }catch(UnsupportedEncodingException e){
                e.printStackTrace();
            }
        }
    }
}
