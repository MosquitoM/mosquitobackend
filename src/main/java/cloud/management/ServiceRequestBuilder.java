package cloud.management;

import com.sun.deploy.services.Service;
import com.sun.istack.internal.Nullable;
import model.DockerCloudService;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jahmale on 6-6-2016.
 */
public class ServiceRequestBuilder {

    private static String request = "";

    private final static String imageParam = "image";
    private final static String nameParam = "name";
    private final static String amtContainersParam = "target_num_containers";
    private final static String run_cmdParam = "run_command";
    private final static String restartParam = "autorestart";
    private final static String destroyParam = "autodestroy";
    private final static String envarParam = "container_envvars";

    public static HashMap<String, String> container_envars;

    private String image;

    public ServiceRequestBuilder(){}

    /**
     * creates a new String request, which should be applied to a createService call from the DockerCloudClient
     * @param image the image, which should be available on DockerHub, to create the service with
     * @param name (optional) the name to apply to the service, the default will be the 'image'
     * @param amountContainers (optional) the amount of containers that should be created for the service, the default is 1
     * @param run_command (optional) the run command to start the service
     * @param autodestroy (optional) whether the service should destroy once it has been stopped.possible values are:
     *                    'ON_SUCCESS', 'ALWAYS', the default is 'OFF', check the following link for more information:
     *                    https://docs.docker.com/docker-cloud/apps/auto-destroy/
     * @param autorestart (optional) whether the service should restart once it has been stopped.possible values are:
     *                    'ON_FAILURE', 'ALWAYS', the default is 'OFF', check the following link for more information:
     *                    https://docs.docker.com/docker-cloud/apps/autorestart/
     * @return a string which should be applied to the createService call from DockerCloudClient to create a new service
     */
    public String createNewRequest(String image, @Nullable String name, @Nullable int amountContainers,
                                       @Nullable String run_command, @Nullable String autodestroy,
                                       @Nullable String autorestart){
        this.image = image;

        Map<String, Object> parameterList = new HashMap<>();
        parameterList.put(nameParam, name);
        parameterList.put(amtContainersParam, amountContainers);
        parameterList.put(run_cmdParam, run_command);
        parameterList.put(destroyParam, autodestroy);
        parameterList.put(restartParam, autorestart);
        parameterList.put(envarParam, container_envars);

        finalizeRequest(parameterList);
        return request;
    }

    /**
     * will add a new set of envars for the service to be created, the envars will only be applied if this method has been called
     * before the createNewRequest method has been called
     * @param envars the envars, placed in a hashmap in key, value format
     */
    public void addNewEnvars(HashMap<String, String> envars){

        if(container_envars == null){
            container_envars = new HashMap<>();
        }
        container_envars.putAll(envars);
    }

    /**
     * Will add a new pair of envars for the service to be created, the envars will only be applied if this method has been called
     * before the createNewRequest method has been called
     * @param key the key of the envar
     * @param value the value for the envar
     */
    public void addNewEnvars(String key, String value){
        if(container_envars == null){
            container_envars = new HashMap<>();
        }
        container_envars.put(key, value);
    }

    private void finalizeRequest(Map<String, Object> parameterList){
        request += "\""+imageParam+"\":\""+image+"\"";

        parameterList.forEach((k,v) -> addParameter(k, v));

        request = "{"+request+"}";
    }

    private void addParameter(String key, Object value){
        if(value instanceof String){
            if(((String) value).isEmpty() || value == ""){
                return;
            }
            request += ", \""+key+"\":\""+value+"\"";
        }
        else if(value instanceof Integer){
            request += ", \""+key+"\":"+value;
        }
        else if(value instanceof HashMap){
            request += ",\""+envarParam+"\": [";
            ((HashMap) value).forEach((finalKey,finalValue) ->
                    request += "{\"key\": \""+finalKey+"\", \"value\": \""+finalValue+"\"},");
            request = request.substring(0, request.length()-1);
            request += "]";
        }
        else{return;}
    }

}
