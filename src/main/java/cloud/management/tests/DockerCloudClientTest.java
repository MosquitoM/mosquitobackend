package cloud.management.tests;

import cloud.management.DockerCloudClient;
import cloud.management.ServiceRequestBuilder;
import config.Config;
import javafx.util.Pair;
import model.DockerCloudContainer;
import model.DockerCloudNode;
import model.DockerCloudService;
import org.apache.commons.lang.ObjectUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by jahmale on 7-6-2016.
 */
public class DockerCloudClientTest {

    private String DOCKER_NODE_UUID = "e2f2995b-4ecf-4909-ad7e-fa916e48779d";
    private String  FAKE_UUID = "e2f2995b-4ecf-4909-ad7e-fa916e4xxxxx";
    private Pair<String, String> envarPair1 = new Pair<>("TestKey", "TestValue");
    private String serviceCreationRequest;
    private String SERVICE_UUID_TO_ENTER;
    private String CONTAINER_UUID_TO_ENTER;


    private DockerCloudClient testClient;

    @Before
    public void applyTestParams(){
        //make sure to have the credentials saved to config.properties
//        try{
//            Config.getInstance().saveCredentials(new Pair<>("mosquitome", "9aa850e6-4ba0-4c14-91fe-71823cb8e26a"));
//        }catch(Exception e){
//            e.printStackTrace();
//            fail();
//        }

        String image = "mosquitome/busybox";
        String name="mosquitoTest"+System.currentTimeMillis();
        int num_containers = 4;
        String autodestroy = "OFF";
        String autorestart = "";
        String runCommand = null;

        ServiceRequestBuilder SRB = new ServiceRequestBuilder();
        SRB.addNewEnvars(envarPair1.getKey(), envarPair1.getValue());
        SRB.addNewEnvars(envarPair1.getKey()+"2",envarPair1.getValue()+"2");

        serviceCreationRequest = SRB.createNewRequest(image, name, num_containers, runCommand, autodestroy, autorestart);
        Pair<String, String> creds = Config.getInstance().getCredentials();
        testClient = DockerCloudClient.getInstance().addCredentials(creds.getKey(), creds.getValue());
        SERVICE_UUID_TO_ENTER = "42d89e61-51e2-46bf-90b0-e207b077d2f2";
        CONTAINER_UUID_TO_ENTER = "fb3db209-43fc-47b0-b1e3-bfd66e7dbfb2";
    }

    @Test
    public void tryAuthenticate(){
        //Make sure to be authenticated to get a valid response
        boolean authenticated = testClient.authenticate();
        assertTrue("asserting authentication", authenticated);
    }

    @Test
    public void tryListAllNodes(){
        DockerCloudNode node = testClient.listAllNodes().get(0);
        assertEquals(node.getUUID(), DOCKER_NODE_UUID);
    }

    @Test
    public void tryGetAllContainerWithUUID(){
        List<DockerCloudContainer> containers = testClient.listAllContainers("42d89e61-51e2-46bf-90b0-e207b077d2f2");
        System.out.println(containers);
        assertTrue(containers != null);
    }

    @Test
    public void tryGetAllContainers(){
        List<DockerCloudContainer> containers = testClient.listAllContainers();
        System.out.println(containers);
        assertTrue(containers != null);
    }

    @Test
    public void tryGetExistingContainer(){
        DockerCloudContainer container = testClient.getExistingContainer(CONTAINER_UUID_TO_ENTER);
        System.out.println(container);
        assertEquals(container.getUUID(), CONTAINER_UUID_TO_ENTER);
    }

    @Test (expected = NullPointerException.class)
    public void shouldFailGetExistingContainer(){
        String UUIDused = FAKE_UUID;
        testClient.getExistingContainer(UUIDused);
    }

    @Test
    public void tryStartContainer(){
        DockerCloudContainer container = testClient.startContainer(CONTAINER_UUID_TO_ENTER);
        assertTrue("container was succesfully started", container.getUUID().equals(CONTAINER_UUID_TO_ENTER));
        assertEquals(container.getUUID(),CONTAINER_UUID_TO_ENTER);
    }

    @Test
    public void tryStopContainer(){
        DockerCloudContainer container = testClient.stopContainer(CONTAINER_UUID_TO_ENTER);
        assertEquals(container.getUUID(),CONTAINER_UUID_TO_ENTER);
    }

    @Test
    public void tryTerminateContainer(){
        DockerCloudContainer container = testClient.terminateContainer(CONTAINER_UUID_TO_ENTER);
        assertEquals(container.getUUID(),CONTAINER_UUID_TO_ENTER);
    }

    @Test
    public void tryGetContainer(){
        DockerCloudContainer container = testClient.getExistingContainer(CONTAINER_UUID_TO_ENTER);
        assertEquals(container.getUUID(),CONTAINER_UUID_TO_ENTER);
    }

    @Test
    public void tryGetAllServices(){
        List<DockerCloudService> services = testClient.listServices();
        System.out.println(services);
        assertTrue(services != null);
    }

    @Test
    public void tryGetExistingService(){
        DockerCloudService service = testClient.getExistingService(SERVICE_UUID_TO_ENTER);
        System.out.println(service);
        assertEquals(service.getUUID(), SERVICE_UUID_TO_ENTER);

    }

    @Test
    public void tryCreateService(){
        DockerCloudService service = testClient.createService(serviceCreationRequest);
        assertTrue("service was succesfully created", service != null);
    }

    @Test
    public void tryStartService(){
        DockerCloudService service = testClient.startService(SERVICE_UUID_TO_ENTER);
        assertEquals(service.getUUID(), SERVICE_UUID_TO_ENTER);
    }

    @Test
    public void tryStopService(){
        DockerCloudService service = testClient.stopService(SERVICE_UUID_TO_ENTER);
        assertEquals(service.getUUID(), SERVICE_UUID_TO_ENTER);
    }

    @Test
    public void tryTerminateService(){
        DockerCloudService service = testClient.terminateService(SERVICE_UUID_TO_ENTER);
        assertEquals(service.getUUID(), SERVICE_UUID_TO_ENTER);
    }

}
