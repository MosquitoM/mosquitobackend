package cloud.management;
import model.DockerCloudNode;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by jahmale on 20-5-2016.
 */
public interface DockerCloudBase {

    String INCORRECT_AUTH = "{\"detail\":\"Incorrect authentication credentials.\"}";

    String DOCKER_BASE_URL = "https://cloud.docker.com";
    String DOCKER_AUTH_HEADER = "Authorization";

    String START = "start";
    String STOP = "stop";
    String DELETE = "delete";

    Boolean authenticate();
}
