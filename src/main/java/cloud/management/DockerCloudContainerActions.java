package cloud.management;

import model.DockerCloudContainer;

import java.util.List;

/**
 * Created by jahmale on 20-5-2016.
 */
public interface DockerCloudContainerActions {
    String DOCKER_CONTAINER_PATH = "/api/app/v1/container";

    /**
     * Gets all current and recently terminated containers
     * @return a list of DockerCloudContainers
     */
    List<DockerCloudContainer> listAllContainers();

    /**
     * Gets all current and recently terminated containers
     * @param filter parameter which will filter the list, this filter can be
     *               a UUID(of a service or node), a container name,
     *               resource URI of a service, resource URI of a node,
     *               the state such as 'starting', 'running', 'stopping',
     *               'stopped', 'terminating', 'terminated'
     *
     *               (current implementation only accepts a service uuid)
     * @return a list of DockerCloudContainers
     */
    List<DockerCloudContainer> listAllContainers(String filter);

    /**
     * Gets an existing container
     * @param uuid The uuid of the container you wish to return
     * @return a DockerCloudContainer
     */
     DockerCloudContainer getExistingContainer(String uuid);

    /**
     * Starts a stopped container
     * @param uuid The uuid of the container that needs to be started
     * @return a DockerCloudContainer
     */
    DockerCloudContainer startContainer(String uuid);

    /**
     * Stops a running container
     * @param uuid the uuid of the container that needs to be started
     * @return a DockerCloudContainer
     */
    DockerCloudContainer stopContainer(String uuid);

    /**
     * Terminates the specified container. This is not reversible. All data stored in the container will be permanently deleted.
     * @param uuid the uuid of the container to terminate
     * @return the terminated DockerCloudContainer(as a sign of confirmation, the container however the container won't be reachable
     * after termination)
     */
    DockerCloudContainer terminateContainer(String uuid);
}
