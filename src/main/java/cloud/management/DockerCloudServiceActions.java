package cloud.management;

import com.sun.istack.internal.Nullable;
import javafx.util.Pair;
import model.DockerCloudService;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

/**
 * Created by jahmale on 20-5-2016.
 */
public interface DockerCloudServiceActions {

    String DOCKER_SERVICE_PATH = "/api/app/v1/service";

    /**
     * Creates a new service, of which its containers will be deployed onto the emptiest docker cloud node available
     * @param JSONRequest a String in JSON format, obtained from the ServiceBuilder.createService() method
     * @return returns a DockerCloudService from the newly created service
     */
    DockerCloudService createService(String JSONRequest);

    /**
     * Lists all of the user's current and recently terminated services
     * @return a list of DockerCloudServices
     */
    List<DockerCloudService> listServices();

    /**
     * Gets an existing service with the specified uuid
     * @param uuid the service uuid to check for
     * @return a DockerCloudService
     */
    DockerCloudService getExistingService(String uuid);

    /**
     * Starts all containers in the stopped (or partly running) service with the specified uuid
     * @param uuid the service uuid to check for
     * @return a DockerCloudService
     */
    DockerCloudService startService(String uuid);

    /**
     * Stops all containers in the running (or partly running) service with the specified uuid
     * @param uuid the service uuid to check for
     * @return a DockerCloudService
     */
    DockerCloudService stopService(String uuid);

    /**
     * Terminate all the containers in a service and the service itself. This is not reversible. All the data stored in all containers of the service will be permanently deleted.
     * @param uuid the service uuid to check for
     * @return a DockerCloudService(as a sign of confirmation, the container however the container won't be reachable
     * after termination)
     */
    DockerCloudService terminateService(String uuid);

}
