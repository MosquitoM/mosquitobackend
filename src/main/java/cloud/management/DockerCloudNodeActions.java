package cloud.management;

import model.DockerCloudNode;

import java.util.List;

/**
 * Created by jahmale on 11-6-2016.
 */
public interface DockerCloudNodeActions {

    String DOCKER_NODE_PATH = "/api/infra/v1/node";

    /**
     * lists all the nodes on Dockercloud
     * @return a list of DockerCloudNodes
     */
    List<DockerCloudNode> listAllNodes();

}
