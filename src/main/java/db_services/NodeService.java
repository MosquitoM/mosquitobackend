package db_services;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.table.TableUtils;
import model.Node;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Yusuf on 19-5-2016.
 */
public class NodeService implements DockerObject<Node> {
    private Dao<Node,String> nodeDAO;

    public NodeService(){
        try {
            TableUtils.createTableIfNotExists(jdbc,Node.class);
            nodeDAO = DaoManager.createDao(jdbc, Node.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int add(Node model) throws SQLException {
        return nodeDAO.create(model);
    }

    @Override
    public Node get(String id) throws SQLException {
        return nodeDAO.queryForId(id);
    }

    @Override
    public List<Node> getAll() throws SQLException {
        return nodeDAO.queryForAll();
    }

    public int edit(Node model) throws SQLException {
        return nodeDAO.update(model);

    }

    @Override
    public int delete(Node model) throws SQLException {
        return nodeDAO.delete(model);
    }

    @Override
    public int deleteAll() throws SQLException {
        return TableUtils.clearTable(jdbc,NodeService.class);
    }
}
