package db_services;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import database.DatabaseSingleton;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Yusuf on 19-5-2016.
 */
interface DockerObject <T>  {

    JdbcConnectionSource jdbc = DatabaseSingleton.getConnection();

    /**
     * Adds model object to database using ORMLite
     * @param model Model class that will be mapped
     * @return Amount of rows affected by query
     * @throws SQLException
     */
    public int add(T model) throws SQLException;

    /**
     * Get model object from database associated by id
     * @param id Id
     * @return Model class that corresponds with id
     * @throws SQLException
     */
    public T get(String id)throws SQLException;

    /**
     * Returns all objects in table
     * @return List of objects
     * @throws SQLException
     */
    public List<T> getAll()throws SQLException;

    /**
     * Delete model object from table
     * @param model model object that will be deleted
     * @return number of rows that will be affected
     * @throws SQLException
     */
    public int delete(T model)throws SQLException;

    /**
     * Clear all data out of table
     * @return number of rows that will be affected
     * @throws SQLException
     */
    public int deleteAll()throws SQLException;
}
