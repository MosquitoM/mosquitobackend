package model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import lombok.Data;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jahmale on 5-6-2016.
 */
@Data
public class DockerCloudService extends CloudModelBase<DockerCloudService> {
    private String UUID;
    private String IMAGE;
    private String HOSTNAME;
    private String STARTED_DATETIME;
    private String STOPPED_DATETIME;

    private int NUM_CONTAINERS;
    private String SERVICE_STATE;
    private int RUNNING_CONTAINERS;
    private int STOPPED_CONTAINERS;

    public DockerCloudService(){
    }

    @Override
    public ArrayList<DockerCloudService> createObjectList(JSONArray jsonArray) {
        return super.createObjectList(jsonArray);
    }

    @Override
    public DockerCloudService createObject(JSONObject jsonService){
        DockerCloudService newService = new DockerCloudService();

        newService.setUUID(jsonService.getString("uuid"));
        newService.setIMAGE(jsonService.getString("image_name"));
        newService.setHOSTNAME(jsonService.getString("name"));

        if(!jsonService.isNull("started_datetime")){
            newService.setSTARTED_DATETIME(jsonService.getString("started_datetime"));
        }else{newService.setSTARTED_DATETIME("");}

        if(!jsonService.isNull("stopped_datetime")){
            newService.setSTOPPED_DATETIME(jsonService.getString("stopped_datetime"));
        }else{newService.setSTOPPED_DATETIME("");}

        newService.setNUM_CONTAINERS(jsonService.getInt("current_num_containers"));
        newService.setSERVICE_STATE(jsonService.getString("state"));
        newService.setRUNNING_CONTAINERS(jsonService.getInt("running_num_containers"));
        newService.setSTOPPED_CONTAINERS(jsonService.getInt("stopped_num_containers"));

        return newService;
    }

    @Override
    protected HashMap<String, String> addEnvars(JSONArray envars) {
        return null;
    }

}