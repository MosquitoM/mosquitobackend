package model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import lombok.Data;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jahmale on 7-6-2016.
 */
@Data
public class DockerCloudNode extends CloudModelBase<DockerCloudNode>{
    private String UUID;
    private int MEMORY;
    private String DEPLOYED_DATETIME;

    private int CURRENT_NUM_CONTAINERS;

    private String STATE;

    private String PUBLIC_IP;

    @Override
    public ArrayList<DockerCloudNode> createObjectList(JSONArray jsonArray){
        return super.createObjectList(jsonArray);
    }

    @Override
    public DockerCloudNode createObject(JSONObject jsonNode){
        DockerCloudNode newNode = new DockerCloudNode();

        newNode.setUUID(jsonNode.getString("uuid"));
        newNode.setMEMORY(jsonNode.getInt("memory"));
        newNode.setPUBLIC_IP(jsonNode.getString("public_ip"));
        newNode.setDEPLOYED_DATETIME(jsonNode.getString("deployed_datetime"));
        newNode.setSTATE(jsonNode.getString("state"));
        newNode.setCURRENT_NUM_CONTAINERS(jsonNode.getInt("current_num_containers"));

        return newNode;
    }

    @Override
    protected HashMap<String, String> addEnvars(JSONArray envars) {
        return null;
    }

}
