package model;

import lombok.Data;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jahmale on 5-6-2016.
 */
@Data
public class DockerCloudContainer extends CloudModelBase<DockerCloudContainer>{
    private String UUID;
    private String IMAGE;
    private String HOSTNAME;
    private String NODE;
    private String DEPLOYED_DATETIME;

    private String STARTED_DATETIME;
    private String STOPPED_DATETIME;
    private String CONTAINER_STATE;
    private HashMap<String, String> CONTAINER_ENVARS;

    public DockerCloudContainer(){}

    @Override
    public ArrayList<DockerCloudContainer> createObjectList(JSONArray jsonArray) {
        return super.createObjectList(jsonArray);
    }

    @Override
    public DockerCloudContainer createObject(JSONObject jsonContainer) {
        DockerCloudContainer newContainer = new DockerCloudContainer();

        newContainer.setUUID(jsonContainer.getString("uuid"));
        newContainer.setIMAGE(jsonContainer.getString("image_name"));
        newContainer.setHOSTNAME(jsonContainer.getString("name"));
        newContainer.setNODE(jsonContainer.getString("node"));

        if(!jsonContainer.isNull("started_datetime")){
            newContainer.setSTARTED_DATETIME(jsonContainer.getString("started_datetime"));
        }else{newContainer.setSTARTED_DATETIME("");}

        if(!jsonContainer.isNull("stopped_datetime")){
            newContainer.setSTOPPED_DATETIME(jsonContainer.getString("stopped_datetime"));
        }else{newContainer.setSTOPPED_DATETIME("");}

        if(!jsonContainer.isNull("container_envvars")){
            newContainer.setCONTAINER_ENVARS(addEnvars(jsonContainer.getJSONArray("container_envvars")));
        }else{newContainer.setCONTAINER_ENVARS(new HashMap<>());}

        String completeDeployed = jsonContainer.getString("deployed_datetime");
                newContainer.setDEPLOYED_DATETIME(completeDeployed.substring(
                        0,completeDeployed.length()-5));
        newContainer.setCONTAINER_STATE(jsonContainer.getString("state"));

        return newContainer;
    }
}
