package model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jahmale on 10-6-2016.
 */
public abstract class CloudModelBase<T> {

    public CloudModelBase(){}

    /**
     * will create a new list of objects of type T
     * @param jsonArray a jsonArray, such as the DockerCloud API's jsonArray return of a list of containers,
     *                  which will be used to create the objects within the list
     * @return a list existing out of objects of type T
     */
    protected ArrayList<T> createObjectList(JSONArray jsonArray){
        ArrayList<T> objectList = new ArrayList<>();
        jsonArray.forEach(singleObject ->
                objectList.add(createObject((JSONObject) singleObject)));
        return objectList;
    }

    /**
     * will create a new object of type T
     * @param jsonObject a jsonObject, such as the DockerCloud API's jsonObject return of an existing container,
     *                   which will be used to create an object
     * @return a newly created object of type T
     */
    protected T createObject(JSONObject jsonObject){
        //This method should always be overwritten
       return null;
    }

    /**
     * will iteratively add envars to the object being built
     * @param envars the envars in JSONArray format
     * @return a hashmap existing out of envars in key, value format
     */
    protected HashMap<String, String> addEnvars(JSONArray envars){
        HashMap<String, String> envarMap = new HashMap<>();
        for(Object envar: envars){
            JSONObject jsonEnvar = (JSONObject) envar;
            envarMap.put(jsonEnvar.getString("key"), jsonEnvar.getString("value"));
        }
        return envarMap;
    }
}
