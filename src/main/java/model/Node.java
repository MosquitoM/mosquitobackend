package model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import lombok.Data;

/**
 * Created by Yusuf on 1-5-2016.
 */
@Data
@DatabaseTable(tableName = "NODE")
public class Node {
    @DatabaseField
    private String URL;
    @DatabaseField(id = true)
    private String NAME;

    @Override
    public String toString(){
        return "NAME " + NAME + " URL " + URL;
    }

}
