package model;

import lombok.Data;
import lombok.ToString;

/**
 * Created by Yusuf on 30-4-2016.
 */
@ToString
@Data
public class Container {
    private String ID;
    private String image;
    private String names;

}
