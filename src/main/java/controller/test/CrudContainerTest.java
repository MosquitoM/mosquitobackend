package controller.test;

import com.shekhargulati.reactivex.docker.client.representations.ContainerInspectResponse;
import com.shekhargulati.reactivex.docker.client.utils.Strings;
import controller.CrudContainer;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;

import static org.junit.Assert.*;

/**
 * Created by arjen on 8-6-2016.
 */
public class CrudContainerTest {

    private CrudContainer crudContainer;
    private String deleteContainerTestId = "bcd610aff166c77101d70c5eb7041e3288f7395a27beea338c497517d96eb514";
    private String deleteContainerNodeTestId = "default";
    private String readContainerTestId = "5ad3be65c193dc5ea3a1fbdc7afed856492ec3f6635fd62256ef587287ee0263";
    private String readContainerNodeTestId = "default";

    @Before
    public void tryGetCrudContainer(){
        try {
            crudContainer = new CrudContainer();

        }catch (SQLException e){
            e.printStackTrace();
        }

    }

    @Test
    public void tryReadContainer(){

        assertEquals(readContainerTestId,crudContainer.readContainer(readContainerTestId,readContainerNodeTestId).id());
    }

    @Test
    public void tryDeleteContainer(){

        assertNotNull(crudContainer.readContainer(deleteContainerTestId, deleteContainerNodeTestId));
        crudContainer.deleteContainer(deleteContainerTestId,deleteContainerNodeTestId);
        assertNull(crudContainer.readContainer(deleteContainerTestId,deleteContainerNodeTestId));
    }

}
