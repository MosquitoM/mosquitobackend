package controller.test;

import controller.OverviewContainers;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import java.sql.SQLException;

/**
 * Created by arjen on 8-6-2016.
 */
public class OverviewContainersTest {

    OverviewContainers overviewContainers;
    @Before
    public void  tryOverviewContainers(){

        try{
            overviewContainers = new OverviewContainers();

        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Test
    public void tryGetActiveContainers(){

        assertEquals(2,overviewContainers.getActiveContainers().size());
    }

    @Test
    public void tryGetPassiveContainers(){

        assertEquals(2,overviewContainers.getPassiveContainers().size());
    }

}
