package controller;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.shekhargulati.reactivex.docker.client.RxDockerClient;
import com.shekhargulati.reactivex.docker.client.representations.*;
import com.shekhargulati.reactivex.docker.client.representations.Image;
import com.shekhargulati.reactivex.rxokhttp.ServiceException;
import com.spotify.docker.client.DockerCertificateException;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.DockerException;
import com.spotify.docker.client.messages.Container;
import com.spotify.docker.client.messages.ContainerConfig;
import com.spotify.docker.client.messages.ContainerCreation;
import com.spotify.docker.client.messages.ContainerInfo;
import database.DatabaseSingleton;
import db_services.NodeService;
import docker_client.DClient;
import docker_client.RxClient;
import model.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Arjen on 02-6-2016.
 */
public class Scale {

    private HashMap<Node, RxDockerClient> dockerClientMap = new HashMap<Node, RxDockerClient>();

    private List<Node> nodeList;
    private RxDockerClient rxDockerClient;
    private Dao<Node, String> nodeDAO;
    private JdbcConnectionSource connectionSource = DatabaseSingleton.getConnection();


    public Scale() throws SQLException {

        nodeDAO = DaoManager.createDao(connectionSource, Node.class);

        nodeList = nodeDAO.queryForAll();

        for (Node node : nodeList) {
            RxDockerClient rxDockerClient = RxClient.changeLocation(node.getURL(), node.getNAME());
            dockerClientMap.put(node, rxDockerClient);
            System.out.println(dockerClientMap);
        }
    }

    /**
     * Create an image based on the container's changes
     * @param containerId Id of the container that will be used to create the new image
     * @return Id of the newly created image
     * @throws DockerException
     * @throws InterruptedException
     */
    public String commitImage(String containerId, String nodeId) throws DockerException, InterruptedException, SQLException, DockerCertificateException {


        DockerClient dockerClient = null;
        for (Map.Entry<Node, RxDockerClient> entry : dockerClientMap.entrySet()) {
            Node node = entry.getKey();
            if (node.getNAME().equals(nodeId)) {

                dockerClient = DClient.getConnection(node.getURL(), node.getNAME());
            }
        }

        ContainerConfig containerConfig = ContainerConfig.builder()
                .build();

        ContainerCreation commitImage = dockerClient.commitContainer(containerId, "", "", containerConfig, "", "");
        String imageId = commitImage.id().split(":")[1].substring(0, 12);

        System.out.println(imageId);
        return imageId;
    }

    /**
     * Store all the images in a repository in .tar format to the containerimages folder your home folder.
     * If the folder does not exists, it will create the folder.
     * @param id Id of the image that will be stored
     * @param nodeId id of the node where the image is stored
     */
    public void saveImage(String id,String nodeId) {

        Path path = Paths.get(System.getenv("USERPROFILE") + "\\containerimages");

        RxDockerClient rxDockerClient;

        for (Map.Entry<Node, RxDockerClient> entry : dockerClientMap.entrySet()) {
            Node node = entry.getKey();
            if (node.getNAME().equals(nodeId)) {
                rxDockerClient = entry.getValue();

                if (!Files.exists(path)) {
                    try {
                        Files.createDirectory(path);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                rxDockerClient.getTarballForAllImagesInRepository(id, path);
            }
        }
    }

    /**
     * Load the image in .tar format from local folder and create and start new container
     * using that image on remote location.
     * @param imageId Name of the .tar file containing the image
     * @return ID of the newly created Docker container
     */
    public String loadImage(String imageId) throws SQLException {

        String containerID = "";
        System.out.println("LOADING IMAGE TO " + rxDockerClient.getApiUri());
        rxDockerClient.loadImagesAndTagsTarball(Paths.get(System.getenv("USERPROFILE") + "\\containerimages\\" + imageId + ".tar"));

        DockerContainerRequest containerRequest = new DockerContainerRequestBuilder()
                .setImage(imageId)
                .createDockerContainerRequest();

        DockerContainerResponse containerResponse = rxDockerClient.createContainer(containerRequest);
        rxDockerClient.startContainer(containerResponse.getId());
        containerID = containerResponse.getId();


        return containerID;
    }

    /**
     * Check the amount of running containers on the nodes
     * and get connection with the one who has the lowest amount
     * @return the name of the current node that has the lowest amount of containers
     */
    public String getConnection() {

        Node currentNode = null;
        int currentNodeSize = -1;

        // for each node get a list of running containers
        for (Map.Entry<Node, RxDockerClient> entry : dockerClientMap.entrySet()) {
            Node node = entry.getKey();

            RxDockerClient rxDockerClient = RxClient.changeLocation("http://" + node.getURL(), node.getNAME());
            List<DockerContainer> runningContainers = rxDockerClient.listRunningContainers();
            if (runningContainers.size() < currentNodeSize || currentNodeSize == -1) {
                currentNodeSize = runningContainers.size();
                currentNode = node;
            }
        }

        rxDockerClient = RxClient.getConnection("http://" + currentNode.getURL(), currentNode.getNAME());

        return currentNode.getNAME();
    }
}

