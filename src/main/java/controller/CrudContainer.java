package controller;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.shekhargulati.reactivex.docker.client.RxDockerClient;
import com.shekhargulati.reactivex.docker.client.representations.*;
import com.shekhargulati.reactivex.rxokhttp.ServiceException;
import database.DatabaseSingleton;
import docker_client.RxClient;
import model.Node;

import java.sql.SQLException;
import java.util.*;

/**
 * Created by arjen on 4-5-2016.
 * The use of this class is for the basic crud commando's of the container
 */
public class CrudContainer {


    private HashMap<Node, RxDockerClient> dockerClientMap = new HashMap<Node,RxDockerClient>();
    private RxDockerClient rxDockerClient;

    public CrudContainer()throws SQLException {

        JdbcConnectionSource connectionSource = DatabaseSingleton.getConnection();
        Dao<Node, String>  nodeDAO = DaoManager.createDao(connectionSource, Node.class);
        List<Node> nodeList = nodeDAO.queryForAll();

        for (Node node:nodeList) {
            RxDockerClient rxDockerClient = RxClient.changeLocation(node.getURL(),node.getNAME());
            dockerClientMap.put(node,rxDockerClient);
            System.out.println(dockerClientMap);
        }
    }

    /**
     * Start a container
     * @param containerId Id of the container you want to start
     * @param nodeId  Id of the node where the container is located
     */
    public void startContainer(String containerId, String nodeId){

        if(!containerId.equals("")) {

            RxDockerClient rxDockerClient;

            for (Map.Entry<Node, RxDockerClient> entry : dockerClientMap.entrySet()) {
                Node node = entry.getKey();

                if (node.getNAME().equals(nodeId)) {
                    rxDockerClient = entry.getValue();

                    rxDockerClient.startContainer(containerId);
                }
            }
        }
    }

    /**
     * Stop a container
     * @param containerId Id of the container you want to stop
     * @param nodeId Id of the node where the container is located
     */
    public void stopContainer(String containerId,String nodeId){

        if(!containerId.equals("")) {

            RxDockerClient rxDockerClient;

            for (Map.Entry<Node, RxDockerClient> entry : dockerClientMap.entrySet()) {
                Node node = entry.getKey();

                if (node.getNAME().equals(nodeId)) {
                    rxDockerClient = entry.getValue();

                    rxDockerClient.stopContainer(containerId,0);
                }
            }
        }
    }

    /**
     * Get the information of a container
     * @param containerId Id of the container where you want information from
     * @param nodeId Id of the node where the container is located
     * @return  the information of the container if the container can't be found return null
     */
    public ContainerInspectResponse readContainer(String containerId, String nodeId){

        ContainerInspectResponse containerInfo = null;

        try {
            if(!containerId.equals("")) {

                RxDockerClient rxDockerClient;

                for (Map.Entry<Node, RxDockerClient> entry : dockerClientMap.entrySet()) {
                    Node node = entry.getKey();

                    if (node.getNAME().equals(nodeId)) {
                        rxDockerClient = entry.getValue();

                        containerInfo = rxDockerClient.inspectContainer(containerId);
                    }
                }
            }
        }catch (IllegalStateException | ServiceException e){

            e.printStackTrace();
            containerInfo = null;
        }
        return containerInfo;
    }

    /**
     * Delete a container
     * @param containerId Id of the container you want to delete
     * @param nodeId Id of the node where the container is located
     */
    public void deleteContainer(String containerId, String nodeId) {

        if(!containerId.equals("")) {

            RxDockerClient rxDockerClient;

            for (Map.Entry<Node, RxDockerClient> entry : dockerClientMap.entrySet()) {
                Node node = entry.getKey();

                if (node.getNAME().equals(nodeId)) {
                    rxDockerClient = entry.getValue();

                    rxDockerClient.removeContainer(containerId);

                }
            }
        }
    }

    public void getLogs(String containerId, String nodeId) {

        if (!containerId.equals("")) {

            RxDockerClient rxDockerClient;

            for (Map.Entry<Node, RxDockerClient> entry : dockerClientMap.entrySet()) {
                Node node = entry.getKey();
                if (node.getNAME().equals(nodeId)) {
                    rxDockerClient = entry.getValue();

                    rx.Observable<String> logsObs = rxDockerClient.containerLogsObs(containerId);
                    logsObs.subscribe(System.out::println,
                            e -> System.err.println("Encountered exception >> " + e.getMessage()),
                            () -> System.out.println("Successfully completed"));
                }
            }
        }
    }


    /**
     * Create a container
     * @param imageId Id of the image you want to use
     * @param containerName the name of how you want to call your new container
     * @param exposedPorts the ports you want to expose
     * @param hostPorts the ports where you want to host the container
     * @return the container id of the new container. Return null if image couldn't be found
     */
    public String createContainer(String imageId,String nodeId, String containerName, String exposedPorts, String hostPorts){

        RxDockerClient rxDockerClient;
        String containerId = null;

        for (Map.Entry<Node, RxDockerClient> entry : dockerClientMap.entrySet()) {
            Node node = entry.getKey();

            if (node.getNAME().equals(nodeId)) {
                rxDockerClient = entry.getValue();

                final Map<String, List<PortBinding>> portBindings = new HashMap<>();

                    List<PortBinding> hostPortBinding = new ArrayList<>();
                    hostPortBinding.add(PortBinding.of("0.0.0.0", hostPorts));
                    portBindings.put(hostPorts, hostPortBinding);


                final HostConfig hostConfig = new HostConfigBuilder().setPortBindings(portBindings).createHostConfig();
                DockerContainerRequest containerRequest = new DockerContainerRequestBuilder()
                        .setImage(imageId)
                        .setHostConfig(hostConfig)
                        .addExposedPort(exposedPorts)
                        .createDockerContainerRequest();

                try{

                    DockerContainerResponse containerResponse =  rxDockerClient.createContainer(containerRequest,containerName);

                    containerId = containerResponse.getId();

                }catch (ServiceException | NullPointerException e){
                    if (e.getMessage().equals("Service returned 409 with message Conflict")){

                        containerId = "Service returned 409 with message Conflict";
                    }
                    if(e.getMessage().equals("Service returned 404 with message Not Found")){

                        containerId = "Service returned 404 with message Not Found";
                    }
                    e.printStackTrace();

                }
            }
        }
        return containerId;
    }

}
