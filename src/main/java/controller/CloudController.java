package controller;

import cloud.management.DockerCloudClient;
import cloud.management.ServiceRequestBuilder;
import config.Config;
import javafx.util.Pair;
import model.DockerCloudContainer;
import model.DockerCloudService;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;

import java.util.HashMap;
import java.util.Map;

import static spark.Spark.get;
import static spark.Spark.post;

/**
 * Created by jahmale on 11-6-2016.
 */
public class CloudController {

    private DockerCloudClient cloudUser;
    private String globalUsername;
    final String STOP = "stop";
    final String START = "start";

    public CloudController(){
        cloudUser = DockerCloudClient.getInstance();
        Pair<String, String> credentials = checkForExistingCredentials();
        if(credentials != null){
            cloudUser.addCredentials(credentials.getKey(), credentials.getValue());
            globalUsername = credentials.getKey();
            cloudUser.authenticate();
        }

        String layout = "templates/layout.vtl";

        get("/cloudOverview", (request, response) -> {
            Map model = new HashMap();
            //cloudUser = DockerCloudClient.getInstance().addCredentials("mosquitome", "9aa850e6-4ba0-4c14-91fe-71823cb8e26a");
            DockerCloudService service;
            String startService = request.queryParams("startService");
            String stopService = request.queryParams("stopService");

            model.put("warning", false);
            doLoginCheck(model);

            if(cloudUser.hasDockerCloudCredentials){
                if(startService != null){
                    service = cloudUser.getExistingService(startService);
                    if(!correctServiceState(START, service)){
                        model.replace("warning",true);
                        model.put("warningMessage", "Can't start service while it is "+service.getSERVICE_STATE());
                    }else {
                        cloudUser.startService(startService);
                    }
                }
                else if(stopService != null){
                    service = cloudUser.getExistingService(stopService);
                    if(!correctServiceState(STOP, service)){
                        model.replace("warning", true);
                        model.put("warningMessage", "Can't stop service while it is "+service.getSERVICE_STATE());
                    }else{
                        cloudUser.stopService(stopService);
                    }
                }
            }
            try{
                if(cloudUser.hasDockerCloudCredentials){
                    model.put("allServices", cloudUser.listServices());
                }
                model.put("template","templates/cloudOverview.vtl");
            }catch(Exception e){

            }
            return  new ModelAndView(model, layout);
        },new VelocityTemplateEngine());


        get("/cloudContainers", (request, response) ->{
            String serviceUUID = request.queryParams("serviceUUID");
            String serviceIMAGE = request.queryParams("serviceIMAGE");
            String startContainer = request.queryParams("startCloudContainer");
            String stopContainer = request.queryParams("stopCloudContainer");
            Map model = new HashMap();
            DockerCloudContainer container;
            try{

                if(startContainer != null){
                    container = cloudUser.getExistingContainer(startContainer);
                    if(!correctContainerState(START, container)){
                        model.replace("warning",true);
                        model.put("warningMessage", "Can't start service while it is "+container.getCONTAINER_STATE());
                    }else {
                        cloudUser.startContainer(startContainer);
                    }
                }
                else if(stopContainer != null){
                    container = cloudUser.getExistingContainer(stopContainer);
                    if(!correctContainerState(STOP, container)){
                        model.replace("warning", true);
                        model.put("warningMessage", "Can't stop service while it is "+container.getCONTAINER_STATE());
                    }else{
                        cloudUser.stopContainer(stopContainer);
                    }
                }

                model.put("serviceUUID", serviceUUID);
                model.put("serviceIMAGE", serviceIMAGE);
                model.put("serviceContainers", cloudUser.listAllContainers(serviceUUID));
                model.put("template", "templates/cloudContainers.vtl");
            }catch(Exception e){
                e.printStackTrace();
            }

            return new ModelAndView(model, layout);
        }, new VelocityTemplateEngine());

        get("/serviceBuilder_form", (request, response) -> {
            Map model = new HashMap();

            doLoginCheck(model);

            //ServiceParameters
            String image = request.queryParams("image");
            if(image != null){

                String name = request.queryParams("serviceName");
                int amount;
                try{
                    amount = Integer.parseInt(request.queryParams("amountContainers"));
                }catch(NumberFormatException e){
                    amount = 1;
                }
                String command = request.queryParams("run_command");
                String autoDestroy = request.queryParams("autodestroy");
                String autoRestart = request.queryParams("autorestart");

                try{
                    ServiceRequestBuilder SRB = new ServiceRequestBuilder();
                    cloudUser.createService(
                            SRB.createNewRequest(
                                    image,name,amount,command,autoDestroy,autoRestart
                            ));
                    model.put("success",true);
                }catch(Exception e){
                    e.printStackTrace();
                    model.put("success", false);
                }

            }

            model.put("template","templates/form/serviceBuilder_form.vtl");
            return  new ModelAndView(model, layout);
        },new VelocityTemplateEngine());

        get("/cloudLogin_form", (request, response) -> {
            Map model = new HashMap();
            String logoutRequest = request.queryParams("logoutFunc");
            if(logoutRequest != null){
                Config.getInstance().removeCredentials();
                cloudUser.hasDockerCloudCredentials = false;
            }

            doLoginCheck(model);
            model.put("template", "templates/form/cloudLogin_form.vtl");

            if(globalUsername != null){
                model.put("username", globalUsername);
            }
            return new ModelAndView(model, layout);
        }, new VelocityTemplateEngine());

        post("/cloudLoggedIn", (request, response) ->{
            Map model = new HashMap();
            String username = request.queryParams("username");
            String password = request.queryParams("password");
            model.put("authenticated", -1);
            if(username != null){
                cloudUser = DockerCloudClient.getInstance().addCredentials(username, password);
                if(!cloudUser.authenticate()){
                    model.put("authenticated",0);
                }else{
                    model.put("authenticated",1);
                    globalUsername = username;
                    model.put("username", globalUsername);
                }
            }
            model.put("template", "templates/cloudLoggedIn.vtl");
            return new ModelAndView(model, layout);
        }, new VelocityTemplateEngine());
    }

    private Boolean correctServiceState(String checkCommand, DockerCloudService service){
        if(checkCommand.equals(STOP)){
            if(service.getSERVICE_STATE().equalsIgnoreCase("not running")
                    ||service.getSERVICE_STATE().equalsIgnoreCase("stopping")
                    ||service.getSERVICE_STATE().equalsIgnoreCase("stopped")
                    ||service.getSERVICE_STATE().equalsIgnoreCase("starting")
                    ||service.getSERVICE_STATE().equalsIgnoreCase("redeploying")
                    ||service.getSERVICE_STATE().equalsIgnoreCase("terminating")
                    ||service.getSERVICE_STATE().equalsIgnoreCase("terminated")){
                return false;
            }else{
                return true;
            }
        } else if (checkCommand.equals(START)) {
            if(service.getSERVICE_STATE().equalsIgnoreCase("partly running")
                    ||service.getSERVICE_STATE().equalsIgnoreCase("running")
                    ||service.getSERVICE_STATE().equalsIgnoreCase("starting")
                    ||service.getSERVICE_STATE().equalsIgnoreCase("redeploying")
                    ||service.getSERVICE_STATE().equalsIgnoreCase("terminating")
                    ||service.getSERVICE_STATE().equalsIgnoreCase("terminated")
                    ||service.getSERVICE_STATE().equalsIgnoreCase("stopping")){
                return false;
            }else{
                return true;
            }
        }
        return false;
    }

    private Boolean correctContainerState(String checkCommand, DockerCloudContainer container){
        if(checkCommand.equals(STOP)){
            if(container.getCONTAINER_STATE().equalsIgnoreCase("not running")
                    ||container.getCONTAINER_STATE().equalsIgnoreCase("stopping")
                    ||container.getCONTAINER_STATE().equalsIgnoreCase("stopped")
                    ||container.getCONTAINER_STATE().equalsIgnoreCase("starting")
                    ||container.getCONTAINER_STATE().equalsIgnoreCase("redeploying")
                    ||container.getCONTAINER_STATE().equalsIgnoreCase("terminating")
                    ||container.getCONTAINER_STATE().equalsIgnoreCase("terminated")){
                return false;
            }else{
                return true;
            }
        } else if (checkCommand.equals(START)) {
            if(container.getCONTAINER_STATE().equalsIgnoreCase("partly running")
                    ||container.getCONTAINER_STATE().equalsIgnoreCase("running")
                    ||container.getCONTAINER_STATE().equalsIgnoreCase("starting")
                    ||container.getCONTAINER_STATE().equalsIgnoreCase("redeploying")
                    ||container.getCONTAINER_STATE().equalsIgnoreCase("terminating")
                    ||container.getCONTAINER_STATE().equalsIgnoreCase("terminated")
                    ||container.getCONTAINER_STATE().equalsIgnoreCase("stopping")){
                return false;
            }else{
                return true;
            }
        }
        return false;
    }

    private void doLoginCheck(Map model){
        if(DockerCloudClient.getInstance().hasDockerCloudCredentials){
            model.put("loggedIn", true);
            return;
        }
        model.put("loggedIn", false);
    }

    private Pair<String, String> checkForExistingCredentials(){
        return Config.getInstance().getCredentials();
    }
}
