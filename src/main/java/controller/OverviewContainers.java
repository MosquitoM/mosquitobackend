package controller;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.shekhargulati.reactivex.docker.client.RxDockerClient;
import com.shekhargulati.reactivex.docker.client.representations.DockerContainer;
import database.DatabaseSingleton;
import docker_client.RxClient;
import model.Node;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by arjen on 2-5-2016.
 * The function of this class is to get an overview of al the running and not running containers
 */


public class OverviewContainers {

    private List<DockerContainer> activeContainerList = new ArrayList<>();
    private HashMap<Node,List<DockerContainer>> activeContainersMap = new HashMap<Node,List<DockerContainer>>();
    private HashMap<Node,List<DockerContainer>> passiveContainersMap = new HashMap<Node,List<DockerContainer>>();
    private HashMap<Node, RxDockerClient> dockerClientMap = new HashMap<Node,RxDockerClient>();

    public OverviewContainers()throws SQLException{

        JdbcConnectionSource connectionSource = DatabaseSingleton.getConnection();
        Dao<Node, String>  nodeDAO = DaoManager.createDao(connectionSource, Node.class);
        List<Node> nodeList = nodeDAO.queryForAll();

        for (Node node:nodeList) {
            RxDockerClient rxDockerClient = RxClient.changeLocation(node.getURL(),node.getNAME());
            dockerClientMap.put(node,rxDockerClient);
            System.out.println(dockerClientMap);
        }
    }

    /**
     * Get a hashmap where the key is the node and where the value is the corresponding list of running containers of that node
     * @return the hashmap of the running container(s)
     */
    public HashMap getActiveContainers(){

        for(Map.Entry<Node, RxDockerClient> entry : dockerClientMap.entrySet()) {
            Node node = entry.getKey();
            RxDockerClient rxDockerClient = entry.getValue();

            List<DockerContainer> activeNodeContainerList = new ArrayList<>();
            activeNodeContainerList.addAll(rxDockerClient.listRunningContainers());

            activeContainerList.addAll(activeNodeContainerList);
            activeContainersMap.put(node,activeNodeContainerList);
        }
        return activeContainersMap;
    }

    /**
     * Get a hashmap where the key is the node and where the value is the corresponding list of passive containers of that node
     * @return the hashmap of the passive container(s)
     */
    public HashMap getPassiveContainers(){

        for(Map.Entry<Node, RxDockerClient> entry : dockerClientMap.entrySet()) {

            Node node = entry.getKey();
            RxDockerClient rxDockerClient = entry.getValue();

            List<DockerContainer> passiveNodeContainerList= new ArrayList<>();
            passiveNodeContainerList.addAll(rxDockerClient.listAllContainers());

               for (int i = 0; i < passiveNodeContainerList.size() - 1; i++) {

                    for(int k = 0 ; k < activeContainerList.size() - 1; k++) {
                        if (passiveNodeContainerList.get(i).getId().equals(activeContainerList.get(k).getId())) {
                            passiveNodeContainerList.remove(i);
                        }
                    }
                }
            passiveContainersMap.put(node,passiveNodeContainerList);
        }
        return passiveContainersMap;
    }
}






