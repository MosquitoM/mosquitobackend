package controller;

import db_services.NodeService;
import model.Node;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static spark.Spark.get;

/**
 * Created by Yusuf on 10-6-2016.
 */
public class NodeController {

    public NodeController(){
        String layout = "templates/layout.vtl";

        // Overview of all the nodes
        get("/nodes", (request, response) -> {
            Map model = new HashMap();
            try {
                NodeService nodeService = new NodeService();
                model.put("nodes", nodeService.getAll());

            } catch (SQLException e) {
                e.printStackTrace();
            }
            model.put("template", "templates/nodes.vtl");

            return new ModelAndView(model,layout);
        }, new VelocityTemplateEngine());

        // Page where you can create a new node
        get("/node_form", (request, response) -> {
            Map model = new HashMap();
            String address = request.queryParams("address");
            String name = request.queryParams("nodename");
            Node node = new Node();
            node.setNAME(name);
            node.setURL("http://" + address);
            try {
                NodeService nodeService = new NodeService();
                if (name != null) {
                    if (nodeService.add(node) == 1) {
                        model.put("success", true);
                    }
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            model.put("template", "templates/form/node_form.vtl");
            return new ModelAndView(model, layout);
        }, new VelocityTemplateEngine());

        get("/nodes_edit", (request, response) -> {
            String nodeName = request.queryParams("nodeName");
            String nodeAddress = request.queryParams("address");

            Node node = new Node();
            Map model = new HashMap();

            try {
                if(nodeAddress != null) {

                    node.setNAME(nodeName);
                    node.setURL(nodeAddress);
                    model.put("node", node);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            model.put("template", "templates/nodes_edit.vtl");

            return new ModelAndView(model,layout);
        }, new VelocityTemplateEngine());
    }


}
