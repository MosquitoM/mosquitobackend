package controller;

/**
 * Created by Yusuf on 9-6-2016.
 */

import db_services.NodeService;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static spark.Spark.get;

public class MigrateController {

    public  MigrateController(final MigrateService migrateService){
        String layout = "templates/layout.vtl";

        // Shows the details of the migrated container
        get("/migrate_confirm", (request, response) -> {
            Map model = new HashMap();
            try {
                String id = request.queryParams("containerID");
                String from = request.queryParams("from");
                String to = request.queryParams("destination");

                migrateService.setNode(from);

                String imageId = migrateService.commitContainer(id);
                String tarFile = migrateService.saveImage(imageId);
                migrateService.loadImage(tarFile, to);
                String containerId = migrateService.createContainer(imageId);

                model.put("containerId", containerId);
                model.put("imageId", imageId);
                model.put("location", to);

            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
            model.put("template", "templates/migrate_confirm.vtl");

            return new ModelAndView(model,layout);
        }, new VelocityTemplateEngine());

        // Page where you can migrate the container
        get("/migrate", (request, response) -> {
            Map model = new HashMap();
            model.put("template", "templates/form/migrate_form.vtl");
            try {
                NodeService nodeService = new NodeService();
                migrateService.setNode("default");
                model.put("containers", migrateService.getContainers());
                model.put("nodes", nodeService.getAll());
            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
            return new ModelAndView(model,layout);
        }, new VelocityTemplateEngine());
    }
}
