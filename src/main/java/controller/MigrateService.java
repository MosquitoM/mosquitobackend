package controller;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import database.DatabaseSingleton;
import model.Container;
import model.Node;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Yusuf on 4-6-2016.
 */
public class MigrateService {
    private HttpClient client = HttpClientBuilder.create().build();
    private Dao<Node, String> nodeDAO;
    private String nodeUrl = "";

    /**
     * Creates new image based on the changes of the container
     * @param containerId Id of the container you want the image based upon on
     * @return Id of the newly created image
     * @throws IOException
     */
    public String commitContainer(String containerId) throws IOException {
        String url = nodeUrl + "/commit?container=" + containerId;
        HttpPost post = new HttpPost(url);
        StringEntity stringEntity = new StringEntity(new JSONObject(new Container()).toString());
        post.setEntity(stringEntity);
        post.setHeader("Content-Type", "application/json");
        HttpResponse response = client.execute(post);

        System.out.println("Committing container " + response.getStatusLine().getStatusCode());
        String responseBody = read(response.getEntity().getContent());
        return responseBody.substring(14,responseBody.length()-2);
    }

    /**
     * Stores all the images in an repository in a tarball in a local folder.
     * @param name Id of the image that will be stored in a tarball.
     * @return The name of the file where the images are stored in.
     * @throws IOException
     */
    public String saveImage(String name) throws IOException {
        HttpGet request = new HttpGet(nodeUrl + "/images/get?names=" + name);
        HttpResponse response = client.execute(request);
        String tarFile =  "C:\\Users\\Y.A\\containerimages\\" + name + ".tar";


        final File imageFile = new File(tarFile);
        imageFile.createNewFile();
        final byte[] buffer = new byte[2048];
        int read;

        try (OutputStream imageOutput = new BufferedOutputStream(new FileOutputStream(imageFile))) {
            try (InputStream imageInput = response.getEntity().getContent()) {
                while ((read = imageInput.read(buffer)) > -1) {
                    imageOutput.write(buffer, 0, read);
                }
            }
        }
        System.out.println("Saving image " + response.getStatusLine().getStatusCode());
        return tarFile;
    }

    /**
     * Load a tarball with a set of images into Docker.
     * @param tarFile Absolute path to the tarball containing the images.
     * @throws IOException
     */
    public void loadImage(String tarFile, String nodeName) throws IOException, SQLException {
        nodeUrl = nodeDAO.queryForId(nodeName).getURL();

        HttpPost post = new HttpPost(nodeUrl + "/images/load");
        post.setHeader("Content-Type", "application/json");

        File fileUpload = new File(tarFile);
        InputStreamEntity streamEntity = new InputStreamEntity(new FileInputStream(fileUpload));
        post.setEntity(streamEntity);

        HttpResponse response = client.execute(post);
        System.out.println("Load image " + response.getStatusLine().getStatusCode());
    }

    /**
     * Creates a new container using an image.
     * @param imageId Id of the image
     * @return Id of the newly created container.
     * @throws IOException
     */
    public String createContainer(String imageId) throws IOException {
        HttpPost post = new HttpPost(nodeUrl + "/containers/create");
        post.setHeader("Content-Type", "application/json");

        Container container = new Container();
        container.setImage(imageId);
        JSONObject jsonObject = new JSONObject(container);

        StringEntity stringEntity = new StringEntity(jsonObject.toString());
        post.setEntity(stringEntity);
        HttpResponse response = client.execute(post);

        System.out.println("Create container " + response.getStatusLine().getStatusCode());
        return read(response.getEntity().getContent()).substring(7,  8 + imageId.length() - 1);
    }

    public static String read(InputStream input) throws IOException {
        try (BufferedReader buffer = new BufferedReader(new InputStreamReader(input))) {
            return buffer.lines().collect(Collectors.joining("\n"));
        }
    }

    public List<Container> getContainers() throws IOException {
        List<Container> containers = new ArrayList<>();

        HttpGet request = new HttpGet(nodeUrl + "/containers/json?all=1");
        request.setHeader("Content-Type", "application/json");
        HttpResponse response = client.execute(request);
        JSONArray jsonArrayContainers = new JSONArray(read(response.getEntity().getContent()));
        for(int i = 0; i < jsonArrayContainers.length(); i++){
            JSONObject jsonContainer = jsonArrayContainers.getJSONObject(i);

            Container container = new Container();
            container.setImage(jsonContainer.getString("Image"));
            container.setID(jsonContainer.getString("Id"));
            container.setNames(jsonContainer.getJSONArray("Names").getString(0));
            containers.add(container);
        }
        return containers;
    }

    public MigrateService() throws SQLException {
        JdbcConnectionSource connectionSource = DatabaseSingleton.getConnection();
        nodeDAO = DaoManager.createDao(connectionSource, Node.class);

    }

    public void setNode(String nodeName) throws SQLException {
        nodeUrl = nodeUrl.concat(nodeDAO.queryForId(nodeName).getURL());
    }

    public void removeContainer(String containerId) throws IOException {
        HttpDelete request = new HttpDelete(nodeUrl + "/containers/" + containerId);
        HttpResponse response = client.execute(request);

        System.out.println("DELETE " + response.getStatusLine().getStatusCode());
    }
            //TODO: Delete container on old location after migration

}