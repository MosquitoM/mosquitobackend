package docker_client;

import com.shekhargulati.reactivex.docker.client.RxDockerClient;

/**
 * Created by Yusuf on 20-5-2016.
 * Creates singleton class where a connection is made with the Docker client using the Remote API client library of
 * Shekhar Gulati. This library makes it possible to write asynchronous methods using observable streams of RxJava.
 */
public class RxClient {
    private static String HOST;
    private static String CERTIFICATES;
    private static RxClient DOCKER_CLIENT;
    private static RxDockerClient CLIENT;

    /**
     * The name and ip address will be used to retrieve the certificates of the node in the local folder of the user
     * @param ip ip address of the node that you want to connect with.
     * @param name name of the node that you want to connect with.
     */
    private RxClient(String ip, String name){
        HOST = ip;
        CERTIFICATES = System.getenv("USERPROFILE") + "\\.docker\\machine\\machines\\" + name;
        setConnection();
    }

    /**
     * Given the ip address and name of the node, a connection will be made using the library
     */
    private static void setConnection(){
        CLIENT = RxDockerClient.newDockerClient(HOST,CERTIFICATES);
    }

    /**
     * If there is already a connection made with a certain node, this will set the connection to another node
     * @param ip ip address of the node that you want to connect with.
     * @param name name of the node that you want to connect with.
     * @return An instance of the Docker client
     */
    public static RxDockerClient changeLocation(String ip, String name){
        HOST = ip;
        CERTIFICATES = System.getenv("USERPROFILE") + "\\.docker\\machine\\machines\\" + name;
        CLIENT = RxDockerClient.newDockerClient(HOST,CERTIFICATES);

        return CLIENT;
    }

    /**
     * Returns a instance of the Docker client, that can be used to work with the Docker client.
     * @param ip ip address of the node that you want to connect with.
     * @param name name of the node that you want to connect with.
     * @return An instance of the Docker client
     */
    public static RxDockerClient getConnection(String ip, String name){
        if(DOCKER_CLIENT == null){
            DOCKER_CLIENT = new RxClient(ip, name);
        }
        return CLIENT;
    }
}
