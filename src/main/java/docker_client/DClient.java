package docker_client;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerCertificateException;
import com.spotify.docker.client.DockerCertificates;
import com.spotify.docker.client.DockerClient;

import java.net.URI;
import java.nio.file.Paths;

/**
 * Creates singleton class where a connection is made with the Docker client using the Remote API client library of
 * Spotify. This class will only be used to create a new image based on the changes of the container.
 * Created by Yusuf on 20-5-2016.
 */
public class DClient {
    private static String HOST;
    private static String CERTIFICATES;
    private static DClient DOCKER_CLIENT;
    private static DockerClient CLIENT;

    /**
     * The name and ip address will be used to retrieve the certificates of the node in the local folder of the user
     * @param ip ip address of the node that you want to connect with.
     * @param name name of the node that you want to connect with.
     * @throws DockerCertificateException
     */
    private DClient(String ip, String name) throws DockerCertificateException {
        HOST = "https://" + ip;
        CERTIFICATES = System.getenv("USERPROFILE") + "\\.docker\\machine\\machines\\" + name;
        setConnection();
    }

    /**
     * Given the ip address and name of the node, a connection will be made using the library
     * @throws DockerCertificateException
     */
    private static void setConnection() throws DockerCertificateException {
        CLIENT = DefaultDockerClient.builder()
                .uri(URI.create(HOST))
                .dockerCertificates(new DockerCertificates(Paths.get(CERTIFICATES)))
                .build();
    }

    /**
     * Returns a instance of the Docker client, that can be used to work with the Docker client.
     * @param ip ip address of the node that you want to connect with.
     * @param name name of the node that you want to connect with.
     * @return An instance of the Docker client
     * @throws DockerCertificateException
     */
    public static DockerClient getConnection(String ip, String name) throws DockerCertificateException {
        if(DOCKER_CLIENT == null){
            DOCKER_CLIENT = new DClient(ip, name);
        }
        return CLIENT;
    }
}