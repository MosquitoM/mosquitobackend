package api;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import database.DatabaseSingleton;
import model.Node;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.stream.Collectors;

/**
 * Created by Yusuf on 9-6-2016.
 */
public class ContainerService {
    private Dao<Node, String> nodeDAO;
    private HttpClient client = HttpClientBuilder.create().build();

    public ContainerService() throws SQLException {
        JdbcConnectionSource connectionSource = DatabaseSingleton.getConnection();
        nodeDAO = DaoManager.createDao(connectionSource, Node.class);
    }


    public String read(InputStream input) throws IOException {
        try (BufferedReader buffer = new BufferedReader(new InputStreamReader(input))) {
            return buffer.lines().collect(Collectors.joining("\n"));
        }
    }

    public JSONArray getContainers(String node) throws SQLException, IOException {
        String nodeUrl = nodeDAO.queryForId(node).getURL();
        HttpGet request = new HttpGet(nodeUrl + "/containers/json?all=1");
        request.setHeader("Content-Type", "application/json");
        HttpResponse response = client.execute(request);
        JSONArray jsonArrayContainers = new JSONArray(read(response.getEntity().getContent()));

        return jsonArrayContainers;
    }
}
