package api;

/**
 * Created by Yusuf on 9-6-2016.
 */

import org.json.JSONArray;

import java.io.IOException;
import java.sql.SQLException;

import static spark.Spark.get;

public class ContainerController {
    private static String API_CONTEXT = "api/v1/";

    public ContainerController(final ContainerService containerService) {

        get(API_CONTEXT + "/location/:name", ((request, response) -> {
            String nodeName = request.params(":name");
            JSONArray containerList = new JSONArray();
            try {
                containerList = containerService.getContainers(nodeName);
            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }

            return containerList;
        }));

    }
}
