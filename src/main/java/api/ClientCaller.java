package api;

import com.sun.istack.internal.Nullable;
import exceptions.MosquitoFailedRequestException;
import javafx.util.Pair;
import org.apache.commons.collections.MultiMap;
import org.json.JSONException;
import org.json.JSONObject;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.Response;

/**
 * Created by jahmale on 9-6-2016.
 */
public class ClientCaller {

    private String BASE_URL;
    private String contentType;
    private Client client;

    /**
     * Constructor for the ClientCaller classes, which is used to make client-side http requests
     * @param baseURL The baseurl for the calls that will be made with this instantiation of the class
     * @param contentType the expected response mime type for all the calls that will be made with this
     *                    instantiation of the class
     */
    public ClientCaller(String baseURL, String contentType){
        this.BASE_URL = baseURL;
        this.client = ClientBuilder.newClient();
        this.contentType = contentType;
    }

    /**
     * HTTPDelete request
     * @param API_CALL The path of the call that will be appended to the BASE_URL provided at the initialization
     *                 of the current instantation of the class
     * @param header a single header that will be applied to the request
     * @return the response of the request in jsonformat
     */
    public JSONObject deleteRequest(String API_CALL, Pair<String, Object> header){
        Response res = client.target( BASE_URL )
                .path( API_CALL+"/" ) // API Module Path
                .request( contentType ) // Expected response mime type
                .header( header.getKey(), header.getValue() )
                .delete();

        String response = res.readEntity(String.class);
        res.close();
        return returnResponse(response);
    }

    /**
     * HTTPDelete request
     * @param API_CALL The path of the call that will be appended to the BASE_URL provided at the initialization
     *                 of the current instantiation of the class
     * @param headers A Map of headers that will be applied to the request
     * @return the response of the request in jsonformat
     */
    public JSONObject deleteRequest(String API_CALL, MultivaluedHashMap<String, Object> headers){
        Response res = client.target( BASE_URL )
                .path( API_CALL+"/" ) // API Module Path
                .request( contentType ) // Expected response mime type
                .headers( headers )
                .delete();

        String response = res.readEntity(String.class);
        res.close();
        return returnResponse(response);
    }

    /**
     * HTTPPost request
     * @param API_CALL The path of the call that will be appended to the BASE_URL provided at the initialization
     *                 of the current instantiation of the class
     * @param header a single header that will be applied to the request
     * @param entity the entity that will be applied to the postrequest in case it needs to be 'posted' to the endpoint
     * @return the response of the request in jsonformat
     */
    public JSONObject postRequest(String API_CALL,Pair<String, Object> header,
                               @Nullable Entity entity){
        Response res = client.target( BASE_URL )
                .path( API_CALL+"/" ) // API Module Path
                .request( contentType ) // Expected response mime type
                .header( header.getKey(), header.getValue() )
                .post(entity);

        String response = res.readEntity(String.class);
        res.close();
        return returnResponse(response);
    }

    /**
     * HTTPPost request
     * @param API_CALL The path of the call that will be appended to the BASE_URL provided at the initialization
     *                 of the current instantiation of the class
     * @param headers a map of headers that will be applied to the request
     * @param entity the entity that will be applied to the postrequest in case it needs to be 'posted' to the endpoint
     * @return the response of the request in jsonformat
     */
    public JSONObject postRequest(String API_CALL, MultivaluedHashMap<String, Object> headers,
                                  @Nullable Entity entity){
        Response res = client.target( BASE_URL )
                .path( API_CALL+"/" ) // API Module Path
                .request( contentType ) // Expected response mime type
                .headers( headers )
                .post(entity);

        String response = res.readEntity(String.class);
        res.close();
        return returnResponse(response);
    }

    /**
     * HTTPGet request
     * @param API_CALL The path of the call that will be appended to the BASE_URL provided at the initialization
     *                 of the current instantiation of the class
     * @param header a single header that will be applied to the request, formed from a Java Pair object
     * @param queryParam a Java Pair object to apply as queryParam to the request
     * @return the response of the request in jsonformat
     */
    public JSONObject getRequest(String API_CALL,Pair<String, Object> header,@Nullable Pair<String, Object> queryParam){
        if(queryParam == null){
            queryParam = new Pair<>("","");
        }
        Response res = client.target( BASE_URL )
                .path( API_CALL+"/" ) // API Module Path
                .queryParam(queryParam.getKey(),queryParam.getValue())
                .request( contentType ) // Expected response mime type
                .header(header.getKey() , header.getValue())
                .get();// The basic authentication header goes here;
        String response = res.readEntity(String.class);
        res.close();
        return returnResponse(response);
    }

    /**
     * HTTPGet request
     * @param API_CALL The path of the call that will be appended to the BASE_URL provided at the initialization
     *                 of the current instantiation of the class
     * @param headers a map of headers that will be applied to the request
     * @param queryParam a Java Pair object to apply as queryParam to the request
     * @return the response of the request in jsonformat
     */
    public JSONObject getRequest(String API_CALL,MultivaluedHashMap<String, Object> headers,
                                 @Nullable Pair<String, Object> queryParam){
        if(queryParam == null){
            queryParam = new Pair<>("","");
        }
        Response res = client.target( BASE_URL )
                .path( API_CALL+"/" ) // API Module Path
                .queryParam(queryParam.getKey(),queryParam.getValue())
                .request( contentType ) // Expected response mime type
                .headers(headers)
                .get();// The basic authentication header goes here;
        String response = res.readEntity(String.class);
        res.close();
        return returnResponse(response);
    }

    private JSONObject returnResponse(String response)throws JSONException{
        JSONObject read = null;
        try{
            read = new JSONObject(response);
        }catch(JSONException e){
            e.printStackTrace();
            System.out.println("Request not completed (correctly), response: ");
            return null;
        }

        try{
            if(read.has("error")){
                throw new MosquitoFailedRequestException();
            }
        }catch(MosquitoFailedRequestException ex){
            System.out.println("wrong input given: "+read.toString());
            ex.printStackTrace();
            return null;
        }

        //System.out.println(read);
        return read;
    }
}
